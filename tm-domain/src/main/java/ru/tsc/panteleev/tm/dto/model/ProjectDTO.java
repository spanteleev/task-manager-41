package ru.tsc.panteleev.tm.dto.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.panteleev.tm.enumerated.Status;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "tm_project")
public class ProjectDTO extends AbstractUserOwnedModelDTO {

    private static final long serialVersionUID = 1;

    @NotNull
    private String name = "";

    @NotNull
    private String description = "";

    @NotNull
    @Enumerated(EnumType.STRING)
    private Status status = Status.NOT_STARTED;

    @NotNull
    @Column(name = "created_dt")
    private Date created = new Date();

    @Nullable
    @Column(name = "begin_dt")
    private Date dateBegin;

    @Nullable
    @Column(name = "end_dt")
    private Date dateEnd;

    @NotNull
    @Override
    public String toString() {
        return id + " : " + name + " : " + description + " : "
                + status.getDisplayName() + " : " + dateBegin + " : " + dateEnd;
    }

}
