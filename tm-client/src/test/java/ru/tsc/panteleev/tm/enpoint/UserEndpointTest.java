package ru.tsc.panteleev.tm.enpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.tsc.panteleev.tm.api.endpoint.IAuthEndpoint;
import ru.tsc.panteleev.tm.api.endpoint.IUserEndpoint;
import ru.tsc.panteleev.tm.api.service.IPropertyService;
import ru.tsc.panteleev.tm.dto.request.user.*;
import ru.tsc.panteleev.tm.marker.SoapCategory;
import ru.tsc.panteleev.tm.dto.model.UserDTO;
import ru.tsc.panteleev.tm.service.PropertyService;

import java.util.UUID;

@Category(SoapCategory.class)
public class UserEndpointTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IUserEndpoint userEndpoint = IUserEndpoint.newInstance(propertyService);

    @NotNull
    private final IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance(propertyService);

    @Nullable
    private String adminToken;

    @Nullable
    private String userToken;

    @Before
    public void getToken() {
        adminToken = authEndpoint.login(new UserLoginRequest("admin", "admin")).getToken();
        userToken = authEndpoint.login(new UserLoginRequest("test", "test")).getToken();
    }

    @After
    public void clear() {
        adminToken = null;
        userToken = null;
    }


    public UserDTO addUser(@Nullable final String login, @Nullable final String password, @Nullable final String email) {
        return userEndpoint.registrationUser(new UserRegistryRequest(login, password, email)).getUser();
    }

    @Test
    public void registrationUser() {
        @NotNull final String login = UUID.randomUUID().toString();
        @NotNull final String password = UUID.randomUUID().toString();
        @NotNull final String email = UUID.randomUUID().toString();
        Assert.assertThrows(Exception.class, () -> addUser("", "", ""));
        Assert.assertThrows(Exception.class, () -> addUser(null, null, null));
        Assert.assertThrows(Exception.class, () -> addUser(login, null, ""));
        Assert.assertThrows(Exception.class, () -> addUser(login, "", null));
        Assert.assertThrows(Exception.class, () -> addUser(login, password, ""));
        Assert.assertThrows(Exception.class, () -> addUser(login, password, null));
        @Nullable final UserDTO user = addUser(login, password, email);
        Assert.assertNotNull(user);
    }

    @Test
    public void lockUser() {
        @NotNull final String login = UUID.randomUUID().toString();
        @NotNull final String password = UUID.randomUUID().toString();
        @NotNull final String email = UUID.randomUUID().toString();
        addUser(login, password, email);
        Assert.assertThrows(Exception.class, () -> userEndpoint.lockUser(new UserLockRequest()));
        Assert.assertThrows(Exception.class, () -> userEndpoint.lockUser(new UserLockRequest("", "")));
        Assert.assertThrows(Exception.class, () -> userEndpoint.lockUser(new UserLockRequest("", login)));
        Assert.assertThrows(Exception.class, () -> userEndpoint.lockUser(new UserLockRequest(userToken, login)));
        Assert.assertThrows(Exception.class,
                () -> userEndpoint.lockUser(new UserLockRequest(adminToken, UUID.randomUUID().toString())));
        Assert.assertNotNull(userEndpoint.lockUser(new UserLockRequest(adminToken, login)));
        Assert.assertThrows(Exception.class, () -> authEndpoint.login(new UserLoginRequest(login, password)));
    }

    @Test
    public void unlockUser() {
        @NotNull final String login = UUID.randomUUID().toString();
        @NotNull final String password = UUID.randomUUID().toString();
        @NotNull final String email = UUID.randomUUID().toString();
        addUser(login, password, email);
        Assert.assertNotNull(userEndpoint.lockUser(new UserLockRequest(adminToken, login)));
        Assert.assertThrows(Exception.class, () -> authEndpoint.login(new UserLoginRequest(login, password)));
        Assert.assertThrows(Exception.class, () -> userEndpoint.unlockUser(new UserUnlockRequest()));
        Assert.assertThrows(Exception.class, () -> userEndpoint.unlockUser(new UserUnlockRequest("", "")));
        Assert.assertThrows(Exception.class, () -> userEndpoint.unlockUser(new UserUnlockRequest("", login)));
        Assert.assertThrows(Exception.class, () -> userEndpoint.unlockUser(new UserUnlockRequest(userToken, login)));
        Assert.assertThrows(Exception.class,
                () -> userEndpoint.unlockUser(new UserUnlockRequest(adminToken, UUID.randomUUID().toString())));
        Assert.assertNotNull(userEndpoint.unlockUser(new UserUnlockRequest(adminToken, login)));
        Assert.assertNotNull(authEndpoint.login(new UserLoginRequest(login, password)));
    }

    @Test
    public void removeUser() {
        @NotNull final String login = UUID.randomUUID().toString();
        @NotNull final String password = UUID.randomUUID().toString();
        @NotNull final String email = UUID.randomUUID().toString();
        addUser(login, password, email);
        Assert.assertThrows(Exception.class, () -> userEndpoint.removeUser(new UserRemoveRequest()));
        Assert.assertThrows(Exception.class, () -> userEndpoint.removeUser(new UserRemoveRequest("", "")));
        Assert.assertThrows(Exception.class, () -> userEndpoint.removeUser(new UserRemoveRequest("", login)));
        Assert.assertThrows(Exception.class, () -> userEndpoint.removeUser(new UserRemoveRequest(userToken, login)));
        Assert.assertNotNull(userEndpoint.removeUser(new UserRemoveRequest(adminToken, login)));
        Assert.assertThrows(Exception.class, () -> authEndpoint.login(new UserLoginRequest(login, password)));
    }

    @Test
    public void updateUserProfile() {
        @NotNull final String login = UUID.randomUUID().toString();
        @NotNull final String password = UUID.randomUUID().toString();
        @NotNull final String email = UUID.randomUUID().toString();
        addUser(login, password, email);
        @Nullable final String token = authEndpoint.login(new UserLoginRequest(login, password)).getToken();
        Assert.assertThrows(Exception.class, () -> userEndpoint.updateUserProfile(new UserUpdateRequest()));
        Assert.assertThrows(Exception.class,
                () -> userEndpoint.updateUserProfile(new UserUpdateRequest("", "", "", "")));
        @NotNull final String firstName = UUID.randomUUID().toString();
        @NotNull final String lastName = UUID.randomUUID().toString();
        @NotNull final String middleName = UUID.randomUUID().toString();
        Assert.assertNotNull(userEndpoint.updateUserProfile(new UserUpdateRequest(token, firstName, lastName, middleName)));
        @Nullable final UserDTO user = authEndpoint.profile(new UserProfileRequest(token)).getUser();
        Assert.assertEquals(firstName, user.getFirstName());
        Assert.assertEquals(lastName, user.getLastName());
        Assert.assertEquals(middleName, user.getMiddleName());
    }

    @Test
    public void changeUserPassword() {
        @NotNull final String login = UUID.randomUUID().toString();
        @NotNull final String password = UUID.randomUUID().toString();
        @NotNull final String email = UUID.randomUUID().toString();
        addUser(login, password, email);
        @Nullable final String token = authEndpoint.login(new UserLoginRequest(login, password)).getToken();
        @NotNull final String passwordNew = UUID.randomUUID().toString();
        Assert.assertThrows(Exception.class, () -> userEndpoint.changeUserPassword(new UserChangePasswordRequest()));
        Assert.assertThrows(Exception.class, () -> userEndpoint.changeUserPassword(new UserChangePasswordRequest("", passwordNew)));
        Assert.assertNotNull(userEndpoint.changeUserPassword(new UserChangePasswordRequest(token, passwordNew)));
        Assert.assertThrows(Exception.class, () -> authEndpoint.login(new UserLoginRequest(login, password)));
        Assert.assertNotNull(authEndpoint.login(new UserLoginRequest(login, passwordNew)));
    }

}
